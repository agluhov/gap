# Мой общий модуль для программ и скриптов
для версии `python 3+`

[![pipeline status](https://gitlab.com/agluhov/gap/badges/master/pipeline.svg)](https://gitlab.com/agluhov/gap/commits/master) [![coverage report](https://gitlab.com/agluhov/gap/badges/master/coverage.svg)](https://gitlab.com/agluhov/gap/commits/master)

Данный модуль имеет вложенную тематическую структуру и позволяет мне повторно использовать написанный мной код в различных прикладных задачах.

 * gap
   * config (Работа с файлами конфигурации)
   * convert
   * filename
   * inet
     * proxy
     * requests
     * urllib2
   * parsers
   * system
   * txtfile
   * zbx

