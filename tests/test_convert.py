#!/usr/bin/python3
# coding: utf-8


# imports
# ------------------------------------------
import unittest
from gap.convert import nginx2unixtimestamp, pftime2datetime
from gap.convert import unixtimestamp2nginx, datetime2pftime
from gap.convert import TZ, seconds_timeformat, pf_fulltimeformat, pf_timeformat, nginx_timeformat


# classes
# ------------------------------------------
class ModuleTest(unittest.TestCase):
    def setUp(self):
        self.nginx_timestamp = 'Tue, 31 Oct 2017 12:00:00 GMT'
        self.pf_timestamp = 'Oct 31 11:00:00'
        self.pf_year = 2017

    def test_constants(self):
        self.assertEqual(TZ, 'Asia/Yekaterinburg')
        self.assertEqual(seconds_timeformat, '%s')
        self.assertEqual(pf_fulltimeformat, '%Y %b %d %H:%M:%S')
        self.assertEqual(pf_timeformat, '%b %d %H:%M:%S')
        self.assertEqual(nginx_timeformat, '%a, %d %b %Y %H:%M:%S GMT')

    def test_nginx2unixtimestamp(self):
        test_unix_timestamp = nginx2unixtimestamp(self.nginx_timestamp)
        test_nginx_timestamp = unixtimestamp2nginx(test_unix_timestamp)
        self.assertEqual(test_nginx_timestamp, self.nginx_timestamp)

    def test_pftime2datetime(self):
        test_pf_datetime = pftime2datetime(self.pf_timestamp, self.pf_year)
        test_pf_timestamp = datetime2pftime(test_pf_datetime)
        self.assertEqual(test_pf_timestamp, self.pf_timestamp)


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    suite = unittest.TestLoader().loadTestsFromTestCase(ModuleTest)
#    unittest.TextTestRunner(verbosity=2).run(suite)

