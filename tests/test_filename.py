#!/usr/bin/python3
# coding: utf-8


# imports
# ------------------------------------------
import unittest
from gap.filename import str_in_filename, is_appconfig, is_disabled


# classes
# ------------------------------------------
class ModuleTest(unittest.TestCase):
    def setUp(self):
        self.filename1 = '/tmp/filename.test'
        self.filename2 = '/tmp/app.json'
        self.filename3 = '/tmp/app.json.disabled'

    def test_filename(self):
        self.assertTrue(str_in_filename(self.filename1, 'test'))

    def test_config(self):
        self.assertTrue(is_appconfig(self.filename2))

    def test_disabled(self):
        self.assertTrue(is_disabled(self.filename3))


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    suite = unittest.TestLoader().loadTestsFromTestCase(ModuleTest)
#    unittest.TextTestRunner(verbosity=2).run(suite)

