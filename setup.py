#!/usr/bin/python3

from setuptools import setup, find_packages

package_dir = {'': 'gap'}

setup(name='gap',
      version='0.4',
      description='Aleksandr Gluhov python code',
      url='https://srv-glory.eurostil.ru/aleksandr.gluhov/gap',
      author='Aleksandr Gluhov',
      author_email='aleksandr.gluhov@stroylandiya.ru',
      license='MIT',
      packages=find_packages(),
      top_level='gap')
