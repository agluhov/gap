#!/usr/bin/python3


# imports
# ------------------------------------------
from gap.txtfile import json_reader


# procedures and functions
# ------------------------------------------
def get_config(config_file, key):
    """
    Function gets config from json by key
    :param config_file: string (file path)
    :param key: string
    :return: dict
    """
    ret_val = None
    try:
        with open(config_file) as config_stream:
            config = json_reader(config_stream)
            ret_val = config[key]
    except:
        # do it through logging system
        pass
        #print('Cannot read data ({0}) from config file: {1}'.format(key, config_file))

    return ret_val


# added: 29.09.2017
def get_settings(config_path):
    """
    Функция, получает все настройки из json файла как один большой словарь и возвращает его
    """
    ret_val = None
    try:
        with open(config_path, 'r') as config_stream:
            ret_val = json_reader(config_stream)

    except:
        # do it through logging system
        pass
        #print('Cannot read data ({0}) from config file: {1}'.format(key, config_file))

    return ret_val


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    pass