#!/usr/bin/python3


# imports
# ------------------------------------------


# constants
# ------------------------------------------
proxy_protocol = 'http'
proxy_host = 'srv-proxy'
proxy_domain = 'eurostil.ru'
proxy_port = 3366

proxy_url = '{}://{}.{}:{}'.format(proxy_protocol, proxy_host, proxy_domain, str(proxy_port))

proxy_settings = {
    'http': proxy_url,
    'https': proxy_url
}


# procedures and functions
# ------------------------------------------


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    pass