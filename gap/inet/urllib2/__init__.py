#!/usr/bin/python3


# imports
# ------------------------------------------
import ssl
from gap.inet.proxy import proxy_settings
from urllib.request import ProxyHandler, HTTPSHandler

# constants
# ------------------------------------------
ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

https_handler = HTTPSHandler(context=ctx)
proxy_handler = ProxyHandler(proxy_settings)


# procedures and functions
# ------------------------------------------


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    pass