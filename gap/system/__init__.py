#!/usr/bin/python3


# imports
# ------------------------------------------
import os
import time
import errno
import psutil
import signal
from subprocess import Popen, PIPE

# procedures and functions
# ------------------------------------------

# procedures and functions
# ------------------------------------------

def generate_path(path):
    """
    Процедура, которая пытается создать указанный путь и вызывает ошибку кроме ситуайции когда указанный путь
    уже был создан.
    :param path: (str) путь
    :return: ---
    """
    try:
        os.makedirs(path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise


def touch(filename, times=None):
    """
    Процедура, аналог системной утилиты touch, созадет указанный файл,
    если указана дата, в необязательном параметре (в формате datetime конечно же),
    то данная дата устанавливается датой создания
    """
    with open(filename, 'a'):
        os.utime(filename, times)


def tail(stream, bufsize=1024, sleeptime=0.1):
    """
    Function that perform reading blocks _buf_size from filestream
    and returns a generator object with tailing lines

    :param stream: opened filestream
    :return: generator object (tailing lines from logfile)
    """
    line = ''
    for buf in iter(lambda: stream.read(bufsize), None):
        if os.linesep in buf:
            for line in (line + buf).splitlines(True) + ['']:
                if line.endswith(os.linesep):
                    yield line

        elif not buf:
            time.sleep(sleeptime)


def set_lock(LOCKFILE):
    """
    Функция, устанавливает .lock мьютекс во временной папке приложения и возвращает true, если успешно
    false если не успешно.
    """
    ret_val = False
    if not get_lock(LOCKFILE):
        touch(LOCKFILE)
        ret_val = True

    return ret_val


def get_lock(LOCKFILE):
    """
    Функция, пытается получить .lock мьютекс во временной папке приложения и возвращает true, если это удалось,
    false, если блокировка не была выставлена
    """
    ret_val = False
    if os.path.isfile(LOCKFILE):
        ret_val = True

    return ret_val


def remove_lock(LOCKFILE):
    """
    Функция, пытается удалить .lock мьютекс во временной папке приложения и возвращает true, если это удалось,
    false, если блокировку снять не удалось
    """
    ret_val = False
    if get_lock(LOCKFILE):
        os.remove(LOCKFILE)
        ret_val = True

    return ret_val


def pslist(name=''):
    """
    Функция, которая возвращает список процессов в определенном формате, с возможностью фильтрации по имени
    :param name: (str) имя процесса (возможно часть имени)
    :return: (list of dicts) {pid: (int) pid, name: (str) имя процесса} Список процессов
    """
    if len(name) > 0:
        return [{'pid': ps.pid, 'name': ps.name()} for ps in psutil.process_iter() if name in ps.name()]
    else:
        return [{'pid': ps.pid, 'name': ps.name()} for ps in psutil.process_iter()]


def ps_security(pid, name):
    """
    Функция, осуществляет проверку переданной ей информации о процесса, в соответствии со своим списком
    :param pid: (int) идентифакатор процесса в системе
    :param name: (str) наименование процесса
    :return: (bool) true/false
    """
    ret_val = False
    protected_processes = [
        {'pid': 1, 'name': 'systemd'},
        {'pid': 0, 'name': 'ssh'},
        {'pid': 0, 'name': 'shorewall'},
        {'pid': 0, 'name': 'pacemaker'},
        {'pid': 0, 'name': 'corosync'},
        {'pid': 0, 'name': 'ospfd'},
        {'pid': 0, 'name': 'openvpn'},
        {'pid': 0, 'name': 'postfix'},
        {'pid': 0, 'name': 'arpwatch'},
        {'pid': 0, 'name': 'bacula'},
        {'pid': 0, 'name': 'asterisk'},
    ]

    for pp in protected_processes:
        ret_val = not ((pid == pp['pid']) or pp['name'] in name)

    return ret_val


def killall(ps_list, kill_signal=signal.SIGTERM):
    '''
    Функция, которая завершает все процессы, которые ей были переданы в формате списка процессов,
    после проверки их функцией ps_security
    :param ps_list: (list of dicts) {pid: (int) pid, name: (str) имя процесса} Список процессов
    :param kill_signal: kill signal (default: SIGTERM)
    :return: dict { status: true/false, killed: количество завершенных, failed: количество неудачных }
    '''
    kill_counter = 0
    fail_counter = 0

    for ps in ps_list:
        if ps_security(ps['pid'], ps['name']):
            os.kill(ps['pid'], kill_signal)
            kill_counter += 1

        else:
            fail_counter += 1

    return {'status': (kill_counter > 0), 'killed': kill_counter, 'failed': fail_counter}


def run_syscmd(*cmd):
    p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    yield p.communicate()


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    pass
