#!/usr/bin/python3


# imports
# ------------------------------------------

# constants
# ------------------------------------------
APPS_DIR = '/usr/local/scripts/'
APPS_TMPDIR = '/var/local/scripts/'
APPS_LOGDIR = '/var/log/scripts/'

# procedures and functions
# ------------------------------------------


# program entry point
# ------------------------------------------
if __name__ == '__main__':
    pass
