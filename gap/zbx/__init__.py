#!/usr/bin/python3


# imports
# ------------------------------------------
import json
from gap.txtfile import json_reader


# procedures and functions
# ------------------------------------------
def zbx_json_gun(data):
    """
    Returns ZABBIX LLD JSON string
    :param data: list or tuple of specially formatted dicts
    :return: string
    """
    response = {}
    response['data'] = data
    return json.dumps(response, indent=2, separators=(',', ':'), ensure_ascii=False)


def save_discovery_file(save_path, data_array):
    """
    Procedure that writes to disk ZABBIX LLD JSON string
    :param save_path: (str) Path where to save a file
    :param data_array: list or tuple of specially formatted dicts
    :return: None
    """

    # TODO: Get deal with errors!!!
    with open(save_path, 'w', encoding='utf8') as fstream:
        fstream.write(zbx_json_gun(data_array))


def discovery(save_path):
    with open(save_path, 'r') as fstream:
        print(fstream.read())


def item(save_path, anchor_name, anchor, item_key):
    if anchor is None or item_key is None:
        print('')
        return

    with open(save_path, 'r') as fstream:
        data = json_reader(fstream)
        for data_item in data['data']:
            if data_item[anchor_name] == anchor:
                try:
                    print(data_item[item_key])
                except KeyError as k_e:
                    print('')


# added 29.09.2017
def item2(save_path, anchor_name1, anchor_name2, anchor1, anchor2, item_key):
    if anchor1 is None or anchor2 is None or item_key is None:
        print('')
        return

    with open(save_path, 'r') as fstream:
        data = json_reader(fstream)
        for data_item in data['data']:
            if data_item[anchor_name1] == anchor1\
                    and data_item[anchor_name2] == anchor2:
                try:
                    print(data_item[item_key])
                except KeyError as k_e:
                    print('')


def discovery_argparse(sys_argv, anchor_name, SAVE_PATH):
    if len(sys_argv) > 1:
        if sys_argv[1] == '-d':
            discovery(SAVE_PATH)

        if sys_argv[1] == '-i':
            if len(sys_argv) == 4:
                item(SAVE_PATH, anchor_name, sys_argv[2], sys_argv[3])


def discovery_argparse2(sys_argv, anchor_name1, anchor_name2, SAVE_PATH):
    if len(sys_argv) > 1:
        if sys_argv[1] == '-d':
            discovery(SAVE_PATH)

        if sys_argv[1] == '-i':
            if len(sys_argv) == 5:
                item2(SAVE_PATH, anchor_name1, anchor_name2, sys_argv[2], sys_argv[3], sys_argv[4])

# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    pass