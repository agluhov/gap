#!/usr/bin/python3
# coding: utf-8


# imports
# ------------------------------------------
import pytz
import datetime


# constants
# ------------------------------------------
TZ = 'Asia/Yekaterinburg'

seconds_timeformat = '%s'
pf_timeformat = '%b %d %H:%M:%S'
pf_fulltimeformat = '%Y %b %d %H:%M:%S'
nginx_timeformat = '%a, %d %b %Y %H:%M:%S GMT'


# procedures and functions
# ------------------------------------------
def pftime2datetime(timestamp, year=0):
    ret_val = datetime.time()

    try:
        if year > 0:
            cur_year = year
        else:
            cur_date = datetime.date.today()
            cur_year = cur_date.year

        full_timestr = '{} {}'.format(cur_year, timestamp)
        ret_val = datetime.datetime.strptime(full_timestr, pf_fulltimeformat)
    except:
        pass

    return ret_val


def datetime2pftime(datetime_time):
    ret_val = datetime.time().strftime(pf_timeformat)
    try:
        ret_val = datetime_time.strftime(pf_timeformat)

    except:
        pass

    return ret_val


def nginx2unixtimestamp(nginx_timestamp):
    ret_val = 0
    try:
        ret_val = int(datetime.datetime.strptime(nginx_timestamp, nginx_timeformat).strftime(seconds_timeformat))

    except:
        pass

    return ret_val


def unixtimestamp2nginx(unix_timestamp):
    ret_val = ''
    try:
        computed_datetime = datetime.datetime.fromtimestamp(unix_timestamp)
        ret_val = str(computed_datetime.strftime(nginx_timeformat))

    except:
        pass

    return ret_val


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    pass