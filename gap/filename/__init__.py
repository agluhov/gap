#!/usr/bin/python3


# imports
# ------------------------------------------


# constants
# ------------------------------------------
# I'm usually using this filename as default
# application/script config filename
_APP_CONFIG = 'app.json'

# just a 'disabled' string
_DISABLED = 'disabled'


# procedures and functions
# ------------------------------------------
def str_in_filename(filename, string):
    """
    Returns true if filename
    :param filename: (str) name of file
    :param string: (str) string to be in the name of a file
    :return: (boolean) True if filename contains the string, False if dont't
    """
    # define default return value
    ret_val = False
    try:
        # compute the result
        ret_val = (string in filename)
    except:
        # got any error
        pass

    return ret_val


def is_appconfig(filename, app_config=_APP_CONFIG):
    """
    Returns True if filename is a application/script config file, False if dont't
    :param filename: (str) name of file
    :param app_config: NOT REQUIRED! application config string unification
    :return: (boolean) True if filename contains the app_config, False if don't
    """
    return str_in_filename(filename, app_config)


def is_disabled(filename, disabled=_DISABLED):
    """
    Returns True if this file is 'disabled', False instead
    :param filename:(str) name of file
    :param disabled: NOT REQUIRED! Disabled string unification
    :return: (boolean) True if if this file is 'disabled', False if dont't
    """
    return str_in_filename(filename, disabled)


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    pass