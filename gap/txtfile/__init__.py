#!/usr/bin/python3


# imports
# ------------------------------------------
import json
import zipfile


# procedures and functions
# ------------------------------------------
def json_reader(fstream):
    """
    Returns loaded json dict from text fstream
    :param fstream: text file stream
    :return: (dict) from JSON
    """
    return json.load(fstream)


def read_file(fstream):
    for line in fstream:
        yield line


# added: 29.09.2017
def gzip(file, zfile=''):
    """
    Функция сжатия переданного ей файла. Если мы указываем необязательный параметр - полный путь до сжатого файла,
    то сжатный файл записывает по переданному имени с установленным именем.
    Функция возвращает полный путь до сжатого файла
    """
    if len(zfile) < 1:
        zfile = '{}.gz'.format(file)

    z = zipfile.ZipFile(zfile, 'w', zipfile.ZIP_DEFLATED, allowZip64=True)
    z.write(file)
    z.close()

    return zfile


# program entry point
# ------------------------------------------
#if __name__ == '__main__':
#    pass